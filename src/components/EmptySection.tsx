import React from "react";

export const EmptySection = () => {
  return (
    <>
      <img
        className="big-heart"
        src="./assets/big-heart/big-heart.svg"
        alt="big heart"
      />
      <p className="favorite-text">You haven't liked any superhero yet</p>
    </>
  );
};
